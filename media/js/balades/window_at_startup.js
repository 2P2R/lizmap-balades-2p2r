lizMap.events.on({

  layersadded: function(e) {
    var html = '';
    html+= '<div class="modal-header"><a class="close" data-dismiss="modal">×</a><h3>Les balades proposées par 2P2R</h3></div>';

    html+= '<div class="modal-body">';
    //html+= $('#metadata').html();
        html+= '<strong>Bienvenue sur le moteur de recherche cartographique des fiches balades de <a href="https://2p2r.org" target="_blank">2P2R</a>.</strong>';
        html+= '<p>Ces fiches vous permettront de vous balader à vélo en Occitanie de la demi-journée à plusieurs jours.</p>'; 
        html+= '<p>Pour chaque tracé sont associés une fiche descriptive et un fichier pour GPS. Pour y accéder, il suffit de :';
        html+='<ul><li>cliquer sur le tracé dans le descriptif qui s\'affiche à gauche</li></ul>';
        html+='<ul><li>cliquer sur le lien de la fiche (pdf) et du fichier trace (gpx) pour les télécharger</li></ul></p>';

        html+='<p>Ces fiches et cette application sont mises à disposition gratuitement et sont le fruit du travail des bénévoles de 2P2R. Nous vous proposons donc de soutenir ce projet en adhérant à l’association <a href="https://2p2r.org/l-asso-10/adherez/" target="_blank">https://2p2r.org/l-asso-10/adherez/</a>.</p>';

        html+='<p>N\'hésitez pas à nous <a href="mailto:contact@2p2r.org?subject=balades">contacter</a> pour améliorer les fiches et même pour proposer de nouveaux itinéraires !</p>';
        html+= '</div>';

        html+= '<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Ok</button></div>';

    $('#lizmap-modal').html(html).modal('show');

  }

});